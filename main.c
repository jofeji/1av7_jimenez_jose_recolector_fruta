//Proyecto de Programaci�n
//1AV7
//Jim�nez Miguel Jos� F�lix
#include<stdio.h>
#define v 20
#define h 60
//funciones
void Portada();
void inicio (int frutax, int frutay, int ninox,int ninoy,char campo[v][h],int canastax,int canastay);
void bordes (char campo [v][h]);
void nino (int ninox, int ninoy, char campo[v][h]);
void canasta(int canastax, int canastay, char campo[v][h]);
void frutaj (int frutax, int frutay, char campo[v][h]);
void leercamp(char campo[v][h]);
void Imprimir (char campo[v][h]);
void Movimiento(char campo[v][h],int *ninox,int *ninoy,int *movninox,int *movninoy,int *frutac);
void actualizar (int frutax, int frutay, int ninox,int ninoy,char campo[v][h],int canastax, int canastay);
void Ciclo (char campo[v][h],int ninox,int ninoy,int movninox,int movninoy,int frutax,int frutay,int canastax, int canastay);
void CONTADOR(char campo[v][h], int *frutac);
void fin_continuar();

int main ()
{
    char campo[v][h];
    char continuar;
    int ninox, ninoy;
    int canastax, canastay;
    int frutax, frutay;
    int movninox, movninoy;
    char Tecla;
    ninox =10;
    ninoy =30;
    canastax = 10;
    canastay = 58;
    movninox =0;
    movninoy =0;
    srand(time(NULL));
    Portada();
    getch();
    system("cls");
    do{
    inicio(frutax,frutay,ninox,ninoy,campo,canastax,canastay);
    Ciclo(campo,ninox,ninoy,movninox,movninoy,frutax,frutay,canastax,canastay);
    system("cls");
    fin_continuar();
    continuar=getch();
    }while (continuar=='c' || continuar=='C');
    return 0;
}
void Portada(){
        printf("\n |||||||||||||||ADVERTENCIA||||||||||||||| \n \n ");
    printf("La pantalla parpadea mucho y esto puede ser una molestia. Gracias por su comprension \n \n \n \n");
    printf("**********Instrucciones**********\n");
    printf("Debes recolectar la fruta que hay en el campo. Para eso simplemente tienes que desplazarte hacia ella \n");
    printf ("Para desplazarte , las teclas son las siguientes: \n");
    printf("W: Mover hacia arriba \nA: Mover hacia la izquierda \nD: Mover hacia la derecha \nS: Mover hacia abajo\n");
    printf ("Favor de desactivar el Bloq mayus\n");
    printf("Tu capacidad de fruta es 5\n");
    printf("El juego acaba cuando recojas 20 frutas\n");
    printf("O ------> Fruta\n");
    printf("H ------> Canasta\n\n");
    printf("Eso es todo. Presione Enter para iniciar.\n\n�Que se divierta!");
}
void inicio (int frutax,int frutay,int ninox,int ninoy,char campo[v][h],int canastax,int canastay){
    bordes (campo);
    nino (ninox,ninoy,campo);
    canasta(canastax,canastay,campo);

    frutaj(frutax, frutay,campo);
}
void bordes (char campo [v][h]){
    int i,j;

    for (i=0 ; i<v ; i++)
    {
        for (j=0 ; j<h ; j++)
        {
            if (i==0 || i==v-1)
            {
            campo[i][j]=205;
            }
            else
            {
                if (j==0 || j==h-1)
                {
                campo[i][j]=186;
                }
                else
                {
                campo[i][j]=' ';
                }
            }
        }
    }
}
void nino (int ninox, int ninoy, char campo[v][h]){
       campo[ninox][ninoy]=254;
}
void canasta(int canastax, int canastay, char campo[v][h]){
    campo[canastax][canastay]='H';
    campo[canastax-1][canastay]='H';
    campo[canastax+1][canastay]='H';
}
void frutaj (int frutax, int frutay,char campo[v][h]){
int b,i;
 b=5;
 i=1;
 while(i<=b)
     {
    frutax=rand()%v-2;
    frutay=rand()%h-2;
    if(campo[frutax][frutay]==' ')
          {
            campo[frutax][frutay]='O';
            i++;
          }
    }
    }
void leercamp(char campo[v][h]){
    int i,j;
    for (i=0 ; i<v ; i++)
    {
        for (j=0 ; j<h ; j++)
        {
        printf("%c",campo[i][j]);
        }
        printf("\n");
    }
}
void  Ciclo(char campo[v][h],int ninox,int ninoy,int movninox,int movninoy,int frutax,int frutay,int canastax, int canastay){
    int frutac,ninob, canastac;
    canastac=0;
    frutac=5;
    ninob=0;
      do{
    Imprimir(campo);
    printf("\t\t + Fruta en el campo = %i \n",frutac);
    printf("\t\t + Fruta en las manos = %i \n", ninob);
    printf("\t\t + Fruta en las canastas= %i \n",canastac);
    printf("---------- %i Frutas para llenar el deposito----------\n\n", 15-canastac);
    if (ninob==5)
    {
        printf ("\t ---------- YA NO PUEDES CARGAR MAS ---------- \n");
    }
            Movimiento(campo,&ninox,&ninoy,&movninox,&movninoy,&frutac);
            CONTADOR(campo, &frutac);
            actualizar (frutax,frutay,ninox,ninoy,campo,canastax,canastay);
            ninob=5-frutac;
            if (ninox==canastax && ninoy==canastay &&frutac == 0){
             canastac=canastac+ninob;
             frutaj(frutax, frutay,campo);
            }
        }while(canastac<15);
}
void Imprimir (char campo[v][h])
{
    system("cls");
    leercamp(campo);
}
void Movimiento(char campo[v][h],int *ninox,int *ninoy,int *movninox,int *movninoy,int *frutac)
{

        char tecla;
        if (1 == 1){
        tecla = getch();
        campo[*ninox][*ninoy]=' ';
        if (tecla == 'w')
        {
            if (*ninox !=1)
            {
                *ninox -= 1;
            }
        }
        if (tecla == 's')
        {
            if (*ninox != v-2)
            {
                *ninox +=1 ;
            }
        }
         if (tecla == 'a')
        {
            if (*ninoy != 1)
            {
                *ninoy -=1 ;
            }
        }
        if (tecla == 'd')
        {
            if (*ninoy != h-2)
            {
                *ninoy +=1 ;
            }
        }
}
}
void CONTADOR(char campo[v][h], int *frutac)
{
    int i,j,cont;
    cont =0;
    for (i=0 ; i<v ; i++)
    {
        for (j=0 ; j<h ; j++)
        {
        if (campo[i][j] == 'O')
        {
            cont ++;
        }
        }
    }
    *frutac = cont;
}
void actualizar (int frutax, int frutay, int ninox,int ninoy,char campo[v][h],int canastax, int canastay)
{
    nino (ninox,ninoy,campo);
    canasta(canastax,canastay,campo);
}
void fin_continuar(){
            printf("\t ---------- �FELICIDADES! ---------- \t \n");
    printf("Presione C para jugar de nuevo o cualquier otra tecla para salir\t \n");
        }
